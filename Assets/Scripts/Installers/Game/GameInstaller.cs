﻿using Game.EquationSolving.Strategy.Linear.Impls;
using Game.Signals;
using Ui.Game;
using Ui.Game.Windows;
using Zenject;

namespace Installers.Game
{
    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            DeclareSignals();
            BindWindows();
            BindServices();
            BindStrategies();
        }

        private void DeclareSignals()
        {
            Container.DeclareSignal<SignalOpenLinearEquationAnswerWindow>();
            Container.DeclareSignal<SignalOpenLinearEquationExplainWindow>();
        }

        private void BindWindows()
        {
            Container.BindInterfacesAndSelfTo<HudWindow>().AsSingle();
            Container.BindInterfacesAndSelfTo<FeatureInDevelopmentWindow>().AsSingle();
            Container.BindInterfacesAndSelfTo<LinearEquationsInputWindow>().AsSingle();
            Container.BindInterfacesAndSelfTo<LinearEquationAnswerWindow>().AsSingle();
            Container.BindInterfacesAndSelfTo<LinearEquationExplainWindow>().AsSingle();
        }

        private void BindServices()
        {
            Container.BindInterfacesTo<GameWindowsManager>().AsSingle();
        }

        private void BindStrategies()
        {
            Container.BindInterfacesTo<LinearEquationSolvingStrategy>().AsSingle();
        }
    }
}