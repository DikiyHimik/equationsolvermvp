﻿namespace Core.Constants
{
    public class WindowNames
    {
        public const string HUD = "HudWindow";
        public const string FEATURE_IN_DEVELOPMENT = "Feature in development";
        public const string LINEAR_EQUATIONS_INPUT = "Linear equations input";
        public const string LINEAR_EQUATIONS_ANSWER = "Linear equations answer";
        public const string LINEAR_EQUATIONS_EXPLAIN = "Linear equations explain";
    }
}