﻿using SimpleUi.Abstracts;
using UnityEngine;
using UnityEngine.UI;

namespace Ui.Game.LinearEquationExplain
{
    public class LinearEquationExplainView : UiView
    {
        [Header("Texts")] 
        [SerializeField] private Text currentEquation;
        [SerializeField] private Text explain;

        [Space] 
        [Header("Buttons")] 
        [SerializeField] private Button back;

        public Button Back => back;

        public void SetParameters(string currentEquationText, string explainText)
        {
            currentEquation.text = currentEquationText;
            explain.text = explainText;
        }
    }
}