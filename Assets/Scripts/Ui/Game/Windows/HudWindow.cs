﻿using Core.Constants;
using SimpleUi;
using Ui.Game.Hud;

namespace Ui.Game.Windows
{
    public class HudWindow : WindowBase
    {
        public override string Name => WindowNames.HUD;
        
        protected override void AddControllers()
        {
            AddController<HudController>();
        }
    }
}