﻿using System.Globalization;
using System.Text;
using Core.Extensions;
using Game.EquationSolving.InputData;
using Game.Signals;
using SimpleUi.Abstracts;
using SimpleUi.Signals;
using UniRx;
using Zenject;

namespace Ui.Game.LinearEquationsInput
{
    public class LinearEquationsInputController : UiController<LinearEquationsInputView>, IInitializable
    {
        private readonly SignalBus _signalBus;

        public LinearEquationsInputController(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        public void Initialize()
        {
            View.Back.OnClickAsObservable()
                .Subscribe(OnBackClick)
                .AddTo(View);

            View.Solve.OnClickAsObservable()
                .Subscribe(OnSolveClick)
                .AddTo(View);

            View.A.OnEndEditAsObservable()
                .Subscribe(OnInputFieldEndEdit)
                .AddTo(View);

            View.B.OnEndEditAsObservable()
                .Subscribe(OnInputFieldEndEdit)
                .AddTo(View);

            View.C.OnEndEditAsObservable()
                .Subscribe(OnInputFieldEndEdit)
                .AddTo(View);
        }

        private void OnBackClick(Unit obj) => _signalBus.BackWindow();

        private void OnSolveClick(Unit obj)
        {
            if (!CheckInputDataCorrect())
                return;

            var a = decimal.Parse(View.A.text);
            var b = decimal.Parse(View.B.text);
            var c = decimal.Parse(View.C.text);
            var data = new LinearEquationInputData(a, b, c, ConfigureCurrentEquationAsString());
            _signalBus.Fire(new SignalOpenLinearEquationAnswerWindow(data));
        }

        private void OnInputFieldEndEdit(string value)
        {
            VerifyFieldA();
            View.SetCurrentEquation(ConfigureCurrentEquationAsString());
        }

        private void VerifyFieldA()
        {
            if (string.IsNullOrEmpty(View.A.text))
                return;

            View.SetExceptionalPanelActive(!CheckAFieldCorrect());
        }

        private bool CheckAFieldCorrect() => decimal.Parse(View.A.text, CultureInfo.InvariantCulture) != 0;

        private string ConfigureCurrentEquationAsString()
        {
            var a = !string.IsNullOrEmpty(View.A.text) ? View.A.text : "a";
            var c = !string.IsNullOrEmpty(View.C.text) ? View.C.text : "c";
            string b;
            var bText = View.B.text;

            if (string.IsNullOrEmpty(bText))
                b = "+b";
            else
                b = decimal.Parse(bText) >= 0 ? $"+{bText}" : bText;

            var equation = new StringBuilder()
                .Append(a)
                .Append("*x")
                .Append(b)
                .Append("=")
                .Append(c);

            return equation.ToString();
        }

        private bool CheckInputDataCorrect() =>
            !string.IsNullOrEmpty(View.A.text)
            && !string.IsNullOrEmpty(View.B.text)
            && !string.IsNullOrEmpty(View.C.text)
            && CheckAFieldCorrect();
    }
}