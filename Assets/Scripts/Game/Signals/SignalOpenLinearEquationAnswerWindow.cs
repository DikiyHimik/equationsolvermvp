﻿using Game.EquationSolving.InputData;

namespace Game.Signals
{
    public readonly struct SignalOpenLinearEquationAnswerWindow
    {
        public readonly LinearEquationInputData Data;

        public SignalOpenLinearEquationAnswerWindow(LinearEquationInputData data)
        {
            Data = data;
        }
    }
}