﻿using Core.Constants;
using SimpleUi;
using SimpleUi.Interfaces;
using Ui.Game.LinearEquationAnswer;

namespace Ui.Game.Windows
{
    public class LinearEquationAnswerWindow : WindowBase, IPopUp
    {
        public override string Name => WindowNames.LINEAR_EQUATIONS_ANSWER;
        
        protected override void AddControllers()
        {
            AddController<LinearEquationAnswerController>();
        }
    }
}