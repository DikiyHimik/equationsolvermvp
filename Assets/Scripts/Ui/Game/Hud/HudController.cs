﻿using SimpleUi.Abstracts;
using SimpleUi.Signals;
using Ui.Game.Windows;
using UniRx;
using Zenject;

namespace Ui.Game.Hud
{
    public class HudController : UiController<HudView>, IInitializable
    {
        private readonly SignalBus _signalBus;

        public HudController(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        public void Initialize()
        {
            View.Linear.OnClickAsObservable()
                .Subscribe(OnLinearClick)
                .AddTo(View);
            
            View.Quadratic.OnClickAsObservable()
                .Subscribe(OnQuadraticClick)
                .AddTo(View);
            
             View.Cubic.OnClickAsObservable()
                .Subscribe(OnCubicClick)
                .AddTo(View);
            
             View.FourthDegree.OnClickAsObservable()
                .Subscribe(OnFourthDegreeClick)
                .AddTo(View);
            
             View.System.OnClickAsObservable()
                .Subscribe(OnSystemsClick)
                .AddTo(View);
            
             View.Matrix.OnClickAsObservable()
                .Subscribe(OnMatrixClick)
                .AddTo(View);
        }

        private void OnLinearClick(Unit obj) => _signalBus.OpenWindow<LinearEquationsInputWindow>();
        
        private void OnQuadraticClick(Unit obj) => _signalBus.OpenWindow<FeatureInDevelopmentWindow>();
        
        private void OnCubicClick(Unit obj) => _signalBus.OpenWindow<FeatureInDevelopmentWindow>();
        
        private void OnFourthDegreeClick(Unit obj) => _signalBus.OpenWindow<FeatureInDevelopmentWindow>();
        
        private void OnSystemsClick(Unit obj) => _signalBus.OpenWindow<FeatureInDevelopmentWindow>();
        
        private void OnMatrixClick(Unit obj) => _signalBus.OpenWindow<FeatureInDevelopmentWindow>();
    }
}