﻿using SimpleUi.Abstracts;
using UnityEngine;
using UnityEngine.UI;

namespace Ui.Game.Hud
{
    public class HudView : UiView
    {
        [Header("Buttons")] 
        [SerializeField] private Button linear;
        [SerializeField] private Button quadratic;
        [SerializeField] private Button cubic;
        [SerializeField] private Button fourthDegree;
        [SerializeField] private Button system;
        [SerializeField] private Button matrix;

        public Button Linear => linear;

        public Button Quadratic => quadratic;

        public Button Cubic => cubic;

        public Button FourthDegree => fourthDegree;

        public Button System => system;

        public Button Matrix => matrix;
    }
}