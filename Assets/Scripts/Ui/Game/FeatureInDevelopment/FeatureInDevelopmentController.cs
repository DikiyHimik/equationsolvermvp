﻿using SimpleUi.Abstracts;
using SimpleUi.Signals;
using UniRx;
using Zenject;

namespace Ui.Game.FeatureInDevelopment
{
    public class FeatureInDevelopmentController : UiController<FeatureInDevelopmentView>, IInitializable
    {
        private readonly SignalBus _signalBus;

        public FeatureInDevelopmentController(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        public void Initialize()
        {
            View.CloseButton.OnClickAsObservable()
                .Subscribe(OnCloseClick)
                .AddTo(View);
            
            View.BackgroundCloseButton.OnClickAsObservable()
                .Subscribe(OnCloseClick)
                .AddTo(View);
        }

        private void OnCloseClick(Unit obj) => _signalBus.BackWindow();
    }
}