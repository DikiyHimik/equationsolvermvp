﻿using Core.Constants;
using SimpleUi;
using Ui.Game.LinearEquationsInput;

namespace Ui.Game.Windows
{
    public class LinearEquationsInputWindow : WindowBase
    {
        public override string Name => WindowNames.LINEAR_EQUATIONS_INPUT;
        
        protected override void AddControllers()
        {
            AddController<LinearEquationsInputController>();
        }
    }
}