﻿using Game.EquationSolving.InputData;

namespace Game.EquationSolving.Strategy.Linear
{
    public interface ILinearEquationSolvingStrategy
    {
        string Solve(LinearEquationInputData data);
    }
}