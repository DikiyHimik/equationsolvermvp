﻿using System;
using Game.EquationSolving.InputData;
using Game.EquationSolving.Strategy.Linear;
using Game.Signals;
using SimpleUi.Abstracts;
using SimpleUi.Signals;
using Ui.Game.Windows;
using UniRx;
using Zenject;

namespace Ui.Game.LinearEquationExplain
{
    public class LinearEquationExplainController : UiController<LinearEquationExplainView>, IInitializable
    {
        private readonly ILinearEquationSolvingStrategy _linearEquationSolvingStrategy;
        private readonly SignalBus _signalBus;
        
        private LinearEquationInputData _inputData;

        public LinearEquationExplainController(
            ILinearEquationSolvingStrategy linearEquationSolvingStrategy,
            SignalBus signalBus
        )
        {
            _linearEquationSolvingStrategy = linearEquationSolvingStrategy;
            _signalBus = signalBus;
        }

        public void Initialize()
        {
            View.Back.OnClickAsObservable()
                .Subscribe(OnBackClick)
                .AddTo(View);

            _signalBus.GetStream<SignalOpenLinearEquationExplainWindow>()
                .Subscribe(OnOpen)
                .AddTo(View);
        }

        private void OnBackClick(Unit obj) => _signalBus.BackWindow();

        private void OnOpen(SignalOpenLinearEquationExplainWindow signal)
        {
            _inputData = signal.Data;
            View.SetParameters(_inputData.CurrentEquation, GetExplain());
            _signalBus.OpenWindow<LinearEquationExplainWindow>();
        }

        private string GetExplain()
        {
            var answer = _linearEquationSolvingStrategy.Solve(_inputData);
            var a = _inputData.A;
            var b = _inputData.B;
            var c = _inputData.C;
            var signBeforeB = b >= 0 ? "-" : "+";
            return $"Solved for x:" +
                $"\n" +
                $"\nx = {answer}" +
                $"\n" +
                $"\nGrouping values with \"x\"" +
                $"\non the left side of the equation," +
                $"\nwithout \"x\" on the right side" +
                $"\n" +
                $"\n{a}*x = {c} {signBeforeB} {Math.Abs(_inputData.B)}" +
                $"\n" +
                $"\nCalculate \"x\":" +
                $"\n" +
                $"\nx = {c-b}/{a} = {answer}";
        }
    }
}