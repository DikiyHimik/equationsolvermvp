﻿using SimpleUi.Abstracts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ui.Game.LinearEquationsInput
{
    public class LinearEquationsInputView : UiView
    {
        [Header("Texts")] 
        [SerializeField] private Text currentEquation;

        [Space] 
        [Header("InputFields")] 
        [SerializeField] private TMP_InputField a;
        [SerializeField] private TMP_InputField b;
        [SerializeField] private TMP_InputField c;

        [Space] 
        [Header("Panels")] 
        [SerializeField] private GameObject exceptionalPanel;

        [Space] 
        [Header("Buttons")] 
        [SerializeField] private Button solve;
        [SerializeField] private Button back;

        [Space] 
        [Header("Colors")] 
        [SerializeField] private Color correct;
        [SerializeField] private Color error;

        [Space] 
        [Header("Images")] 
        [SerializeField] private Image aInputField;

        public TMP_InputField A => a;

        public TMP_InputField B => b;

        public TMP_InputField C => c;

        public Button Solve => solve;

        public Button Back => back;

        public void SetExceptionalPanelActive(bool isActive)
        {
            exceptionalPanel.SetActive(isActive);
            aInputField.color = isActive ? error : correct;
        }

        public void SetCurrentEquation(string equationText) => currentEquation.text = equationText;
    }
}