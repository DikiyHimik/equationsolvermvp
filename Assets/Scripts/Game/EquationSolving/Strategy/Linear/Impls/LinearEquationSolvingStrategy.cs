﻿using System.Globalization;
using Game.EquationSolving.InputData;

namespace Game.EquationSolving.Strategy.Linear.Impls
{
    public class LinearEquationSolvingStrategy : ILinearEquationSolvingStrategy
    {
        public string Solve(LinearEquationInputData data)
        {
            var a = data.A;
            var b = data.B;
            var c = data.C;
            var answer = (c - b) / a;
            var roundAnswer = decimal.Round(answer, 20);

            return roundAnswer.ToString(CultureInfo.CurrentCulture);
        }
    }
}