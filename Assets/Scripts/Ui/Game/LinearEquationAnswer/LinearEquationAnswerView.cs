﻿using SimpleUi.Abstracts;
using UnityEngine;
using UnityEngine.UI;

namespace Ui.Game.LinearEquationAnswer
{
    public class LinearEquationAnswerView : UiView
    {
        [Header("Texts")] 
        [SerializeField] private Text currentEquation;
        [SerializeField] private Text solvedX;

        [Space] 
        [Header("Buttons")] 
        [SerializeField] private Button explain;
        [SerializeField] private Button back;

        public Button Explain => explain;

        public Button Back => back;

        public void SetParameters(string currentEquationText, string solvedXText)
        {
            currentEquation.text = currentEquationText;
            solvedX.text = solvedXText;
        }
    }
}