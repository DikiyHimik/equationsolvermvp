﻿using Game.EquationSolving.InputData;
using Game.EquationSolving.Strategy.Linear;
using Game.Signals;
using SimpleUi.Abstracts;
using SimpleUi.Signals;
using Ui.Game.Windows;
using UniRx;
using Zenject;

namespace Ui.Game.LinearEquationAnswer
{
    public class LinearEquationAnswerController : UiController<LinearEquationAnswerView>, IInitializable
    {
        private readonly ILinearEquationSolvingStrategy _linearEquationSolvingStrategy;
        private readonly SignalBus _signalBus;
        
        private LinearEquationInputData _inputData;

        public LinearEquationAnswerController(
            ILinearEquationSolvingStrategy linearEquationSolvingStrategy,
            SignalBus signalBus
            )
        {
            _linearEquationSolvingStrategy = linearEquationSolvingStrategy;
            _signalBus = signalBus;
        }

        public void Initialize()
        {
            View.Back.OnClickAsObservable()
                .Subscribe(OnBackClick)
                .AddTo(View);

            _signalBus.GetStream<SignalOpenLinearEquationAnswerWindow>()
                .Subscribe(OnOpen)
                .AddTo(View);
            
            View.Explain.OnClickAsObservable()
                .Subscribe(OnExplainClick)
                .AddTo(View);
        }

        private void OnBackClick(Unit obj) => _signalBus.BackWindow();

        private void OnExplainClick(Unit obj) => _signalBus.Fire(new SignalOpenLinearEquationExplainWindow(_inputData));

        private void OnOpen(SignalOpenLinearEquationAnswerWindow signal)
        {
            _inputData = signal.Data;
            var answer = _linearEquationSolvingStrategy.Solve(_inputData);
            var solving = $"Solved for x:\n\nx = {answer}";
            View.SetParameters(_inputData.CurrentEquation, solving);
            _signalBus.OpenWindow<LinearEquationAnswerWindow>();
        }
    }
}