﻿using System;
using TMPro;
using UniRx;

namespace Core.Extensions
{
    public static class TMPInputFieldRxExtensions
    {
        public static IObservable<string> OnEndEditAsObservable(this TMP_InputField inputField)
        {
            return inputField.onEndEdit.AsObservable();
        }
    }
}