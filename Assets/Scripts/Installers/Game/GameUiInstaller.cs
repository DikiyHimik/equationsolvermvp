﻿using SimpleUi;
using Ui.Game.FeatureInDevelopment;
using Ui.Game.Hud;
using Ui.Game.LinearEquationAnswer;
using Ui.Game.LinearEquationExplain;
using Ui.Game.LinearEquationsInput;
using UnityEngine;
using Zenject;

namespace Installers.Game
{
    [CreateAssetMenu(menuName = "Installers/GameUiInstaller", fileName = "GameUiInstaller")]
    public class GameUiInstaller : ScriptableObjectInstaller
    {
        [Header("Canvas")]
        [SerializeField] private Canvas canvas;

        [Space] [Header("Views")]
        [SerializeField] private HudView hudView;
        [SerializeField] private FeatureInDevelopmentView featureInDevelopmentView;
        [SerializeField] private LinearEquationsInputView linearEquationsInputView;
        [SerializeField] private LinearEquationAnswerView linearEquationAnswerView;
        [SerializeField] private LinearEquationExplainView linearEquationExplainView;


        public override void InstallBindings()
        {
            var canvasInstance = Container.InstantiatePrefabForComponent<Canvas>(canvas);
            var canvasTransform = canvasInstance.transform;
            
            Container.BindUiView<HudController, HudView>(hudView, canvasTransform);
            Container.BindUiView<FeatureInDevelopmentController, FeatureInDevelopmentView>(featureInDevelopmentView, canvasTransform);
            Container.BindUiView<LinearEquationsInputController, LinearEquationsInputView>(linearEquationsInputView, canvasTransform);
            Container.BindUiView<LinearEquationAnswerController, LinearEquationAnswerView>(linearEquationAnswerView, canvasTransform);
            Container.BindUiView<LinearEquationExplainController, LinearEquationExplainView>(linearEquationExplainView, canvasTransform);
        }
    }
}