﻿using Core.Constants;
using SimpleUi;
using SimpleUi.Interfaces;
using Ui.Game.FeatureInDevelopment;

namespace Ui.Game.Windows
{
    public class FeatureInDevelopmentWindow : WindowBase, IPopUp
    {
        public override string Name => WindowNames.FEATURE_IN_DEVELOPMENT;
        
        protected override void AddControllers()
        {
            AddController<FeatureInDevelopmentController>();
        }
    }
}