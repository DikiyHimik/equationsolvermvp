﻿using Game.EquationSolving.InputData;

namespace Game.Signals
{
    public readonly struct SignalOpenLinearEquationExplainWindow
    {
        public readonly LinearEquationInputData Data;

        public SignalOpenLinearEquationExplainWindow(LinearEquationInputData data)
        {
            Data = data;
        }
    }
}