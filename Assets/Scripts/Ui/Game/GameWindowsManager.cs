﻿using SimpleUi.Signals;
using Ui.Game.Windows;
using Zenject;

namespace Ui.Game
{
    public class GameWindowsManager : IInitializable
    {
        private readonly SignalBus _signalBus;

        public GameWindowsManager(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        public void Initialize()
        {
            _signalBus.OpenWindow<HudWindow>();
        }
    }
}