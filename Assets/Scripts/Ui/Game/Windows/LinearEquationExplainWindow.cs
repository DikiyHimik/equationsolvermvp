﻿using Core.Constants;
using SimpleUi;
using SimpleUi.Interfaces;
using Ui.Game.LinearEquationExplain;

namespace Ui.Game.Windows
{
    public class LinearEquationExplainWindow : WindowBase, IPopUp
    {
        public override string Name => WindowNames.LINEAR_EQUATIONS_EXPLAIN;
        
        protected override void AddControllers()
        {
            AddController<LinearEquationExplainController>();
        }
    }
}