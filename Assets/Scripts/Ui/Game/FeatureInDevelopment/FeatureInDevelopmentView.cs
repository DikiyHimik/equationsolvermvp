﻿using SimpleUi.Abstracts;
using UnityEngine;
using UnityEngine.UI;

namespace Ui.Game.FeatureInDevelopment
{
    public class FeatureInDevelopmentView : UiView
    {
        [Header("Buttons")] 
        [SerializeField] private Button closeButton;
        [SerializeField] private Button backgroundCloseButton;

        public Button CloseButton => closeButton;

        public Button BackgroundCloseButton => backgroundCloseButton;
    }
}