﻿namespace Game.EquationSolving.InputData
{
    public readonly struct LinearEquationInputData
    {
        public readonly decimal A;
        public readonly decimal B;
        public readonly decimal C;
        public readonly string CurrentEquation;

        public LinearEquationInputData(decimal a, decimal b, decimal c, string currentEquation)
        {
            A = a;
            B = b;
            C = c;
            CurrentEquation = currentEquation;
        }
    }
}